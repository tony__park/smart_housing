class Wstomp {

    // static brokerInfo = {
    //     host: "192.168.1.9",
    //     port: 15674,
    //     user: "eosuser",
    //     pass: "eosuser"
    // };


    constructor(settings) {

        // this.brokerInfo = {
        //     host: "localhost",
        //     port: 15674,
        //     user: "test",
        //     pass: "test"
        // };

        this.brokerInfo = {
            host: "localhost",
            port: 15674,
            user: "test",
            pass: "test"
        };

        this.connected = false;

    }

    // connect = () => {
    //     var promise = new Promise(function (resolve, reject) {
    //         this.rabbit_client.connect(this.brokerInfo.user, this.brokerInfo.pass,
    //             function onConnected() {
    //                 resolve(this.rabbit_client);
    //             }.bind(this),
    //             function onError(e) {
    //                 // console.error("Wstomp : on Error (" + e +")");
    //                 reject(e);
    //             }.bind(this),
    //             '/')
    //     }.bind(this));
    //     return promise;
    // }


    establish = (exchange_name, routing_key, callback) => {
        console.log("wstomp establish.....");
        this.rabbit_client.connect(this.brokerInfo.user, this.brokerInfo.pass,
            /* on connected */() => {
                console.log("consumer connected");
                var stomp_url = "/exchange/" + exchange_name + "/" + routing_key;
                this.rabbit_client.subscribe(stomp_url, function (message) {
                    JSON.stringify(message, null, 2);
                    console.log("wstomp.js : " + message);
                    callback(message);
                });
            }, /* on error */(e) => {
                console.error("consumer wstomp error : " + e);
            });
    }

    establishProducer = (exchange_name, routing_key) => {
        this.rabbit_client.connect(this.brokerInfo.user, this.brokerInfo.pass,
            /* on connected */() => {
                console.log("producer connected");
            }, /* on error */(e) => {
                console.error("producer wstomp error : " + e);
            });
    }


    connect = () => {
        this.conn_url = 'ws://' + this.brokerInfo.host + ":" + this.brokerInfo.port + "/ws";
        this.rabbit_ws = new WebSocket(this.conn_url);
        this.rabbit_client = Stomp.over(this.rabbit_ws);

        return new Promise((resolve, reject) => {
            this.rabbit_client.connect(this.brokerInfo.user, this.brokerInfo.pass, 
                () => {
                    /* on-connect */
                    this.rabbit_client.debug = function(str) {};
                    this.rabbit_client.debug = null;
                    this.rabbit_client.info = ()=>{};
                    console.log("rabbitmq wstomp connected");
                    this.connected = true;
                    resolve(this.rabbit_client);
                },
                (e) => {
                    /* on-error */
                    console.log("[error] ", e);
                    reject(e);
                }
            );
        });
    }


    subscribe = (exchange_name, routing_key, callback) => {
        var stomp_url = "/exchange/" + exchange_name + "/" + routing_key;
        this.rabbit_client.subscribe(stomp_url, function (message) {
            console.log("wstomp.js : " + message);

            callback(message);
        });
    }

    send = (exchange_name, routing_key, message) => {
        var stomp_url = "/exchange/" + exchange_name + "/" + routing_key;
        var smessage = JSON.stringify(message);
        this.rabbit_client.send(stomp_url, { priority: 9 }, smessage);
    }
}



