

var app = angular.module('app', []);



// var robotNames = ["001", "002", "003", "004"]

let rcsId = "wrrobot-1";
let robotId = "robot-1";

let robotNames = [robotId];
let g_brokerInfo = {
  host: "localhost",
  port: 15674,
  user: "test",
  pass: "test"
};

function createRobotList() {
  var robots = []
  robotNames.forEach((item, index) => {
    // var wstomp = new Wstomp({});
    var robot = new Robot(item, g_brokerInfo);
    // console.log(robot.name);
    // robot.setStomp(wstomp);
    robots.push(robot);
  });

  return robots;
}


var __robot_list = null;

app.factory('Holder', function () {
  return {
    value: 0
  };
});

app.factory('BrokerInfo', function () {
  return g_brokerInfo;
});

app.factory('StompInfo', () => {

  return {
    host: 'loalhost',
    port: 15674,
    user: 'test',
    pass: 'test'
  }

});


// app.factory('BrokerInfo', function () {
//   return {
//     host: "192.168.1.9",
//     port: 15674,
//     user: "eosuser",
//     pass: "eosuser"
//   }
// });

app.factory("RobotList", function () {

  if (__robot_list == null) {
    __robot_list = createRobotList();
  }

  return {
    robots: __robot_list
  }

});

// viapoint_1:{id:'1f_viapoint_1_way_1', type:1, pose: { x:15.117, y:-0.876, theta: RobotPose2D.quat2rad(0.045,-0.998), zone:1}},

app.factory("POIs", function () {


  // pois = {
  //   poi1: { id: 'poi1', type: 1, pose: { x: 15.117, y: -0.876 }, theta: { x: 0.0, y: 0.0, z: 0.045, w: -0.998 }, zone: 1, name: "1층 일반 경유지_1" },
  //   poi2: { id: 'poi2', type: 1, pose: { x: 6.419, y: -0.364, }, theta: { x: 0.0, y: 0.0, z: 0.023, w: -0.999 }, zone: 1, name: "1층 일반 경유지_2" },
  //   poi3: { id: 'poi3', type: 1, pose: { x: 6.419, y: -0.364 }, theta: { x: 0.0, y: 0.0, z: 0.046, w: -0.998 }, zone: 1, name: "1층 일반 경유지_3" },
  //   poi4: { id: 'poi4', type: 53, pose: { x: 16.462, y: 0.524 }, theta: { x: 0.0, y: 0.0, z: 0.686, w: 0.727 }, zone: 1, name: "1층 상차대기지" },
  //   poi5: { id: 'poi5', type: 51, pose: { x: 16.485, y: 1.409 }, theta: { x: 0.0, y: 0.0, z: 0.673, w: 0.738 }, zone: 1, name: "1층 상차지" },
  //   poi6: { id: 'poi6', type: 54, pose: { x: 3.161, y: -0.367 }, theta: { x: 0.0, y: 0.0, z: 0.686, w: 0.727 }, zone: 1, name: "1층 하차대기지" },
  //   poi7: { id: 'poi7', type: 52, pose: { x: 3.227, y: 0.132 }, theta: { x: 0.0, y: 0.0, z: 0.668, w: 0.743 }, zone: 1, name: "1층 하차지" },
  //   poi8: { id: 'poi8', type: 21, pose: { x: 12.999, y: 5.778 }, theta: { x: 0.0, y: 0.0, z: 0.686, w: 0.727 }, zone: 1, name: "1층 엘리베이터 대기지" },
  //   poi9: { id: 'poi9', type: 22, pose: { x: 13.194, y: 8.041 }, theta: { x: 0.0, y: 0.0, z: 0.686, w: 0.727 }, zone: 1, name: "1층 엘리베이터 탑승지" },
  //   poi10: { id: 'poi10', type: 23, pose: { x: 13.039, y: 6.333 }, theta: { x: 0.0, y: 0.0, z: 0.686, w: 0.727 }, zone: 2, name: "2층 엘리베이터 하차지" },
  //   poi11: { id: 'poi11', type: 41, pose: { x: 0, y: 0 }, theta: { x: 0.0, y: 0.0, z: 0.0, w: 1 }, zone: 1, name: "1층 자동문 대기지" },
  //   poi12: { id: 'poi12', type: 42, pose: { x: 0, y: 0 }, theta: { x: 0.0, y: 0.0, z: 0, w: 1 }, zone: 1, name: "1층 자동문 통과지" },
  //   poi13: { id: 'poi13', type: 61, pose: { x: 13.039, y: 6.333 }, theta: { x: 0.0, y: 0.0, z: 0.045, w: -0.998 }, zone: 1, name: "1층 충전지" },
  //   poi14: { id: 'poi14', type: 91, pose: { x: 13.025, y: 1.673 }, theta: { x: 0.0, y: 0.0, z: 0.022, w: -0.999 }, zone: 1, name: "1층 대기지" },
  // }



  pois = {
    poseInit: {
      name : "초기위치",
      pose: 
      { 
        position: { 
          x: 16.3176518616, 
          y: 4.97354204131, 
          z: 0.0 
        }, 
        orientation: { 
          x: 0, 
          y: 0, 
          z: -0.999999511964, 
          w: -0.0009879633092 
        } 
      }
    },
    poseGate: {
      name : "공동현관문 출발",
      pose: {
        position: { 
          x: 15.1121680525, 
          y: -0.633630239531, 
          z: 0.0 
        }, 
        orientation: {
          x: 0,
          y: 0,
          z: -0.99883441474,
          w: 0.0482681254063
        }
      }
    },
    poseGateReturn: {
      name : "공동현관문 복귀",
      pose: {
        position: {
          x: 15.1121680525,
          y: -0.633630239531,
          z: 0.0
        },
        orientation: {
          x: 0.0,
          y: 0.0,
          z:-0.0408205717904,
          w: 0.999166493092
        }
      }
    },
    poseElevator: {
      name : "엘리베이터 출발",
      pose: {
        position: {
          x: 4.56023593453,
          y: -0.382500876149,
          z: 0.0
        },
        orientation: {
          x: 0.0,
          y: 0.0,
          z:-0.363966169972,
          w: 0.033157995153
        }
      }
    },
    poseElevatorReturn: {
      name : "엘리베이터 복귀",
      pose: {
        position: {
          x: 4.56023593453,
          y: -0.382500876149,
          z: 0.0
        },
        orientation: {
          x: 0.0,
          y: 0.0,
          z:-0.0202027725134,
          w: 0.999795903164
        }
      }
    },
    poseFin: {
      name : "세대앞",
      pose: {
        position: {
          x: -5.01943297504,
          y: 0.531373028945,
          z: 0.0
        },
        orientation: {
          x: 0.0,
          y: 0.0,
          z: 0.00791337164833,
          w: 0.999968688784
        }
      }
    }
  }

  return pois;

});