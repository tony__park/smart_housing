/*
    dependancy : THREE.js
*/


class Transform {
    constructor(pixelmapCenter) {
        this.pixelmapCenter = pixelmapCenter;
    }

    robotposeToCanvas(pose, orientation) {
        var pixelmapCenter = this.pixelmapCenter;
        var rdx = pose.x;
        var rdy = pose.y;

        //
        // transform (robot-coord to pixel coord)
        //
        var rpx = rdx / resolution;
        var rpy = rdy / resolution;

        //
        // applying pixel coord
        //
        rpx = pixelmapCenter.x + rpx;
        rpy = pixelmapCenter.y - rpy;


        //
        // transform (robot-coord to canvas coord)
        //
        //
        var euler = new THREE.Euler();
        var quat = new THREE.Quaternion(orientation.x, orientation.y, orientation.z, orientation.w, "XYZ");
        euler = euler.setFromQuaternion(quat, 'XYZ');
        angle = -euler._z + Math.PI / 2;;

        //
        // transform (robot coord to canvas coord)
        //
        // angle = angle + Math.PI / 2;
        // angle = (-1) * angle;
        // angle += Math.PI / 2;
        // angle = (-1) * angle;
        console.log("angle : " + angle * 180 / Math.PI);


        return {
            x: rpx,
            y: rpy,
            angle: angle
        }
    }

    // canvasToRobotPose()

};