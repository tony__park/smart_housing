app.controller('robotCtrl', function ($scope, RobotList, POIs) {

    $scope.targetRobot = null;
    $scope.robotList = RobotList.robots;
    $scope.robotList.forEach(function (item, index) {
        // item.addUpdatedCallback(function (name, message) {
        //     console.log("robotinfo_controller.js [" + name +"] robot info updated");
        //     $scope.$apply();
        // });
        $scope.targetRobot = item;
    });

    $scope.reqResidentInfo = ()=>{

        console.log("reqResidentStatus");

        $scope.targetRobot.requestResidentStatus().then((message) => {
            console.log("재실상황감지요청 : ", message);
            $scope.appendLog(message.body);
            $scope.appendLog("재실자 존재 응답 도착");
        });
    }




    $scope.notiGateReached = async() => {
        console.log("공동현관문도착알림 버튼 클릭");
        let result = await $scope.targetRobot.notiGateReached();
        console.log("공동현관문 도착알림 결과 : ", result);
        $scope.appendLog(`공동현관문도착 알림 ${result.body}`);
        $scope.appendLog(`공동현관문 <font color='red'>문열림</font> 응답 도착`);
    }

    $scope.callElevator = async() => {
        console.log("엘리베이터 호출버튼 클릭");
        
        let elevatorWait = $scope.targetRobot.waitElevatorArriaval();
        let doorOpen = $scope.targetRobot.waitElevatorDoorOpen();

        let result = await $scope.targetRobot.callElevator();
        $scope.appendLog(`엘리베이터호출결과: ${result.body}`);
        // console.log("엘리베이터 호출 결과 코드", result);

        let ew = await elevatorWait;
        // console.log("엘리베이터 도착함 : ", ew);
        $scope.appendLog(`<font color="red">엘리베이터</font> 도착함: ${ew.body}`);

        let dopen = await doorOpen;
        $scope.appendLog(`<font color="red">엘리베이터</font> 문열림: ${dopen.body}`);
        // console.log("엘리베이터 문열림 : ", ew);
    }
    
    $scope.elevatorBoardOn = async() => {
        console.log("엘리베이터 탑승버튼 클릭");
        $scope.appendLog(`<font color="red">탑승알림</font> 엘리베이터`);
        let floorPromise = $scope.targetRobot.waitElevatorFloor(3);
        $scope.appendLog(`엘리베이터 목적지 도착 기다림`);
        let result = await $scope.targetRobot.sendElevatorBoardOn();
        await floorPromise;
        $scope.appendLog(`엘리베이터 목적지층에 도착 함.`);
        $scope.appendLog(`엘리베이터 문열림.`);

    }

    $scope.notiDeliveryArrival = async() => {
        $scope.appendLog("세대앞도착 알림. ");
        $scope.appendLog(`<font color="red"> 세대앞 도착 알림</font> 결과 기다림 `);
        let result = $scope.targetRobot.sendDeliveryArrival();
        await result;
        $scope.appendLog(`<font color='blue'>세대앞 도착 알림</font> <font color='red'>결과 도착</font> `);
    }

    $scope.requestDeliveryCode = async() => {
        console.log("세대앞도착알림버튼 클릭");
        let orderCodeResonse = $scope.targetRobot.waitOrderResponse();
        console.log("재실자인증코드전송");
        $scope.appendLog(`<font color='blue'>재실자 인증코드 전송</font> `);
        await $scope.targetRobot.sendOrderCodeRequest();

        let confirmCode = await orderCodeResonse;
        $scope.appendLog(`재실자 인증코드 <font color='blue'>결과도착</font> `);
        console.log("재실자인증코드요청결과", confirmCode);
    }

    $scope.requestDeliveryFinish = async() => {
        console.log("배송완료 요청 버튼 클릭");
        let finishWait = $scope.targetRobot.waitDeliveryFinishResponse();

        $scope.appendLog(`<font color='blue'>배송완료 승인 </font> 요청`);
        $scope.targetRobot.sendDeliveryFinishRequest();
        
        let finishResult = await finishWait;
        $scope.appendLog(`<font color='blue'>배송완료 승인 </font> 응답도착`);
        console.log("배송완료요청결과 : ", finishResult);
    }

    $scope.appendLog = (data) => {
        let logPane = document.getElementById('right_panel');
        logPane.scrollTop = logPane.scrollHeight + 100;
        let innerHtml = `<div class="row content"><div class="col-sm-12 container"><p>${data}</p></div> </div>`;
        console.log(innerHtml);
        logPane.innerHTML += innerHtml;
    }


    $scope.gotoGate = async() => {
        console.log("공동현관문으로 출발");
        let gatePose = POIs.poseGate;
        $scope.appendLog(`<font color='blue'> 주행시작</font>`);
        await $scope.targetRobot.moveTo(gatePose);
        $scope.appendLog(`목적지 도착`);
    }

    $scope.gotoElevator = async() => {
        console.log("엘리베이터로이동");
        $scope.appendLog(`<font color='blue'> 주행시작</font>`);
        await $scope.targetRobot.moveTo(POIs.poseElevator);
        $scope.appendLog(`목적지 도착`);

    }

    $scope.gotoInit = async () => {
        console.log("초기위치로이동");
        $scope.appendLog(`<font color='blue'> 주행시작</font>`);
        await $scope.targetRobot.moveTo(POIs.poseInit);
        $scope.appendLog(`목적지 도착`);

    }
    
    $scope.gotoFin = () => {
        console.log("택배호출가정으로 이동");
        $scope.targetRobot.moveTo(POIs.poseFin);
    }

    $scope.returnToElevator = () => {
        // console.log("복귀 : 엘리베이터");
        $scope.appendLog(`<font color='blue'> 엘리베이터</font> 복귀 `);
        $scope.targetRobot.moveTo(POIs.poseElevatorReturn);
    }

    $scope.returnToGate = () => {
        console.log("복귀 : 공동현관문");
        $scope.appendLog(`복귀 <font color='blue'> 공동현관문</font>`);
        $scope.targetRobot.moveTo(POIs.poseGateReturn);
    }

    $scope.cancleGoal = () => {
        console.log("cancleGoal 호출");
        $scope.targetRobot.cancleGoal();

    }


});