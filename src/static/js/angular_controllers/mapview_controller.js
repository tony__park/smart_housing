
class MouseController {
    constructor() {
        this.mouseDownFlag = false;

        this.mouseCoord = {
            x: 0,
            y: 0
        };

        this.mouseMoveDiff = {
            x: 0,
            y: 0
        };

        this.offset = {
            x: 0,
            y: 0
        };
    }


    onMouseDown(e) {
        this.mouseDownFlag = true;
        this.mouseCoord.x = e.clientX;
        this.mouseCoord.y = e.clientY;
    }

    onMouseMove(e) {
        if (this.mouseDownFlag) {
            var x = e.clientX;
            var y = e.clientY;

            var dx = x - this.mouseCoord.x;
            var dy = y - this.mouseCoord.y;

            this.offset.x = this.offset.x - dx;
            this.offset.y = this.offset.y - dy;

            this.mouseCoord.x = x;
            this.mouseCoord.y = y;

        }
    }

    onMouseUp(e) {
        this.mouseDownFlag = false;
    }

    isMouseDown() {
        return this.mouseDownFlag;
    }

    getFocusPoint() {
        return this.focusPoint;
    }
};


// angularjs controller start

//angular.module('app', []).controller('mapViewCtrl', function ($document, $scope, $element) {
app.controller('mapViewCtrl', function ($scope, $element,  RobotList, POIs) {
    $scope.l={};
    console.log( $element);

    var config = document.getElementById('server.config');
    var config_json = JSON.parse(config.value);
    // console.log(JSON.stringify(config_json, null, '\t'));
    $scope.config = config_json;

    //
    // declare opttions
    //
    //
    $scope.option = {
        resolution: 0.05,
        robot_radius: 2,
        map_center_pixel: {
            x: 2000,
            y: 2000
        },
        robot_center_origin: {
            x: 100,
            y: 100
        },
        clicked_pose: {
            x: 2000,
            y: 2000
        }
    };

    $scope.pois = POIs;
    $scope.poi_array = Object.values($scope.pois);


    //
    // get canvas reference
    //
    $scope.canvas = document.getElementById('map_canvas');
    $scope.ctx = $scope.canvas.getContext('2d');

    //
    // canvas view-port object initialize
    //
    $scope.camera = new Camera($scope.ctx, { canvas: $scope.canvas });
    $scope.robotList = RobotList.robots;


    $scope.transform = new Transform($scope.option.map_center_pixel, $scope.option.resolution);
    // $scope.robot = new Robot($scope.transform);

    $scope.map = new Map($scope.ctx, "");
    $scope.map.onLoad = function () {
        $scope.draw();
    };


    $scope.map.loadImage();

    //
    // move camera lookAt() point
    //
    //
    $scope.camera.moveTo($scope.option.map_center_pixel.x, $scope.option.map_center_pixel.x);

    $scope.mousectrl = new MouseController();
    var mouseOffset = {
        x: $scope.option.map_center_pixel.x,
        y: $scope.option.map_center_pixel.y,
    };
    $scope.mousectrl.offset = mouseOffset;


    $scope.canvas.onmousedown = function (e) {
        $scope.mousectrl.onMouseDown(e);
    }

    $scope.canvas.onmouseup = function (e) {
        $scope.mousectrl.onMouseUp(e);
    }

    $scope.canvas.onmousemove = function (e) {
        if ($scope.mousectrl.mouseDownFlag) {
            $scope.mousectrl.onMouseMove(e);
            $scope.camera.moveTo($scope.mousectrl.offset.x, $scope.mousectrl.offset.y);
        }
    }

    //
    // draw map and robots
    //
    $scope.draw = function () {
        $scope.ctx.clearRect(0, 0, $scope.map.image.width, $scope.map.image.height);
        $scope.camera.begin();
        $scope.map.draw();
        $scope.map.drawPOIs(POIs, $scope.transform);
        // $scope.robot.draw($scope.ctx);
        // $scope.drawPois();

        $scope.robotList.forEach(function(robot, item){
            robot.draw($scope.ctx, $scope.transform);
        });

        $scope.drawClicked();
        $scope.camera.end();
    }


    $scope.drawClicked = function() {
        var mouseX = $scope.mousectrl.mouseCoord.x;
        var mouseY = $scope.mousectrl.mouseCoord.y;

        // var canvas_margin = canvas.getBoundingClientRect();
        // var canvas = document.getElementById('map_canvas');
        var canvas = $scope.canvas;
        var canvas_margin = canvas.getBoundingClientRect();
        // var canvasX = $(canvas).offset().left;
        // var canvasY = $(canvas).offset().top;

        var canvasX = canvas_margin.x;
        var canvasY = canvas_margin.y;

        var clickedX = mouseX - canvasX;
        var clickedY = mouseY - canvasY;

        var converted = $scope.camera.screenToWorld(clickedX, clickedY);

        $scope.option.clicked_pose.x = converted.x;
        $scope.option.clicked_pose.y = converted.y;


        $scope.ctx.save();
        $scope.ctx.beginPath();
        $scope.ctx.lineWidth = 5;
        $scope.ctx.strokeStyle = '#003300';
        $scope.ctx.arc(converted.x, converted.y, 10, 0, 2 * Math.PI, false);

        $scope.ctx.stroke();
        $scope.ctx.restore();
    };
    let timerId = setInterval(() => $scope.draw(), 100);



    $scope.robotMoveTo = function () {
        console.log("move to clicked");
        var x = $scope.option.clicked_pose.x;
        var y = $scope.option.clicked_pose.y;

        var pose = {
            x: x,
            y: y
        }

        var converted = $scope.transform.canvasToRobotPose(pose, 0.0);
        $scope.robot.gotoGoal(converted.x, converted.y, 0.0);
        console.log(converted.x, converted.y);
    }

    $scope.robotActionControl = function(cmd) {
        var selected_info = $scope.getSelectedInfo();
        selected_info.selected_robot.actionControl(cmd);
    };


    $scope.gotoElevator = () => {
        let robot = robotList[0];
        robot.moveTo(POIs.poseElevator);

    }

    $scope.gotoFin = () => {
        let robot = robotList[0];
        robot.moveTo(POIs.poseFin);

    }

    $scope.gotoInit = () => {
        console.log("초기위치로이동");
        let robot = robotList[0];
        robot.moveTo(POIs.poseInit);
    }


    $scope.gotoPOI = () => {
        var selected_info = $scope.getSelectedInfo();
        selected_info.selected_robot.gotoPOI(selected_info.selected_poi, $scope.transform);
    }


    $scope.autoCharging = () => {
        var selected_info = $scope.getSelectedInfo();
        selected_info.selected_robot.autoCharging(selected_info.selected_poi, $scope.transform);
    }

    $scope.pickup = () => {
        var selected_info = $scope.getSelectedInfo();
        selected_info.selected_robot.pickup(selected_info.selected_poi, $scope.transform);
    }

    $scope.dropOff = () => {
        var selected_info = $scope.getSelectedInfo();
        selected_info.selected_robot.dropOff(selected_info.selected_poi, $scope.transform);
    }

    $scope.uncharging = () => {
        var selected_info = $scope.getSelectedInfo();
        selected_info.selected_robot.uncharging(selected_info.selected_poi, $scope.transform);
    }

    $scope.getInElevator = () => {
        var selected_info = $scope.getSelectedInfo();
        selected_info.selected_robot.getInElevator(selected_info.selected_poi, $scope.transform);
    }

    $scope.getOffElevator = () => {
        var selected_info = $scope.getSelectedInfo();
        selected_info.selected_robot.getOffElevator(selected_info.selected_poi, $scope.transform);
    }


    $scope.wait = () => {
        var selected_info = $scope.getSelectedInfo();
        selected_info.selected_robot.wait(selected_info.selected_poi, $scope.transform);
    }


    $scope.getSelectedInfo = () => {
        var selected_robot = $scope.selected_robot;
        var selected_poi = $scope.selected_poi;


        
        // var robots = Object.assign({}, $scope.robotList);
        selected_robot = parseInt(selected_robot);
        selected_robot = $scope.robotList[selected_robot - 1];
        selected_poi = $scope.pois[selected_poi];

        return {
            selected_robot : selected_robot,
            selected_poi : selected_poi
        }
    }


}); // end of controller


