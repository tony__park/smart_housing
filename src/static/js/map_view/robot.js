
class Robot {
    // constructor(ctx /* canvas context */, transform) {
    constructor(name, rabbitSetting, rosSetting) {

        // this.mqttBroker = new MQTT_Broker();
        // this.mqttBroker.connect();


        /*
            mapview robot configurations
        */
        this.radius = 40;
        this.lidarPixelSize = 3;
        this.lidar = {};

        this.path = [];
        this.pathPixelSize = 3;

        this.ctx = null;
        this.image = new Image();
        this.imageURL = "/static/image/icons/robot-pin-report.png";
        this.image.onload = function () {
            // console.log("robot image loaded")
        }

        this.image.src = this.imageURL;

        this.pose = { x: 0, y: 0 };
        this.orientation = { x: 0, y: 0, z: 0, w: 0 };
        this.robotPoseUpdated = false;
        this.transform = null;

        /*
           rabbit mq settings 
       */
        this.name = name;
        this.consumerX = "wmr2acs.wmr" + this.name;
        this.consumerRK = "xwmr.wmr" + this.name;
        this.producerX = "acs2wmr.wmr" + this.name;
        this.producerRK = "xacs.wmr" + this.name;


        this.updatedCallbacks = [];

        this.status = {
            system: 0,
            servo: false,
            dock: false,
            charging: false,
            hook: false,
            cart: false,
            estop: false,
        };

        this.batteryLevel = 0;

        // sthis.rabbit_client = this.establishRabbit(rabbitSetting);
        // this.establishMQTT(rabbitSetting);
        this.establishStomp();
        this.establishRos();
    }

    // draw robot
    //
    //
    draw = (ctx, transform) => {
        this.ctx = ctx;
        this.transform = transform;
        if (!this.robotPoseUpdated) { return; }


        var transformed = this.transform.robotposeToCanvas(this.pose, this.orientation);


        var cx = transformed.x;
        var cy = transformed.y;
        var angle = transformed.angle;
        var radius = this.radius;
        this.ctx.font = "20px malgun gothic bold";
        this.ctx.fillStyle = "rgba(255, 0, 255, 1)";
        this.ctx.fillText("wmr" + this.name, cx - radius, cy - radius / 2);


        // this.drawPath();

        this.ctx.save();
        this.ctx.translate(cx, cy);
        this.ctx.rotate(angle);
        this.ctx.drawImage(this.image, -radius, -radius, radius * 2, radius * 2);
        this.ctx.fillStyle = "rgba(0, 0, 255, 1)";
        this.drawLidar(this.ctx, cx, cy);
        this.ctx.fillStyle = "rgba(0, 255, 0, 1)";
        // this.drawPath();
        this.ctx.strokeRect(-radius, -radius, radius * 2, radius * 2);
        this.ctx.restore();
    }

    //
    // draw lidar pixel
    //
    //
    drawLidar(ctx, cx, cy) {

        //
        // cx, cy 
        // robot center (x, y)

        if (this.lidar.ranges == undefined) {
            return;
        }

        var ranges = this.lidar.ranges;
        var angle_increment = this.lidar.angle_increment;

        var resolution = 0.05;
        var distance = 0.0;
        var angle = 0.0;

        var x = 0.0;
        var y = 0.0;

        // console.log("draw lidar : " + this.path.length);
        ranges.forEach(function (item, index, array) {
            distance = item / resolution;
            x = distance * Math.cos(index * (-1) * angle_increment + Math.PI / 2);
            y = distance * Math.sin(index * (-1) * angle_increment + Math.PI / 2);
            ctx.fillRect(x, y, this.lidarPixelSize, this.lidarPixelSize);
        }.bind(this));

    }


    drawPath() {
        if (this.path.length < 1) {
            return;
        }
        var poses = this.path;
        poses.forEach(function (item, index, array) {
            var pose = {
                x: item.pose.position.x,
                y: item.pose.position.y
            };

            var orientation = {
                x: 0, y: 0, z: 0, w: 1
            };
            var canvas_coord = this.transform.robotposeToCanvas(pose, orientation);

            var x = canvas_coord.x;
            var y = canvas_coord.y;
            // console.log(poses.length, x, y);
            this.ctx.fillStyle = "rgba(0, 200, 0, 1)";
            this.ctx.fillRect(x, y, this.pathPixelSize, this.pathPixelSize);


        }.bind(this));


    }

    // establishMQTT = (rabbitSetting) => {
    //     let options = {
    //         username : "test",
    //         passwor : "test",
    //     }

    //     this.mqtt = new Paho.MQTT.Client("127.0.0.1", Number(15675), "/ws","myclientid_" + parseInt(Math.random() * 100, 10));
    //     // this.client = new Paho.MQTT.Client("ws://localhost:1883/mqtt", "myClientId" + new Date().getTime());
    //     this.mqtt.connect(
    //         {
    //             onSuccess : ()=>{
    //                 console.log("mqtt connected");

    //                 this.sendSystemStatus();

    //             },
    //             onFailure : (e) => {
    //                 console.log("mqtt failure - ", e);
    //             },  
    //             userName : "test",
    //             password : "test"
    //         }
    //     );

    //     this.mqtt.onConnectionLost = (msg) => {
    //         console.log("MQTT Connectin Lost : ", msg);
    //     }

    // }


    establishRos = () => {
        //
        // ros
        //
        this.ros = new ROSLIB.Ros({
            url: 'ws://localhost:9090'
        });

        let promise = new Promise((resolve, reject) => {
            this.ros.on('connection', () => {
                console.log('[ROS]Connected to websocket server.');
                resolve(this.ros);
            });

            this.ros.on('error', (error) => {
                console.log('[ROS]Error connecting to websocket server: ', error);
                reject(null);
            });

            this.ros.on('[ROS]close', () => {
                console.log('Connection to websocket server closed.');
            });
        });

        promise.then((resolve) => {
            console.log("resolved");
            this.ros = resolve;
        }).catch((reject) => {
            this.ros = null;
        });

        if (this.ros == null) {
            return;
        }


        // robot pose subscriber decl
        var poseListener = new ROSLIB.Topic({
            ros: this.ros,
            name: '/workerbee_navigation/robot_pose',
            messageType: 'geometry_msgs/PoseStamped'
        });


        poseListener.subscribe((message) => {
            let pose = {
                x: message.pose.position.x,
                y: message.pose.position.y,
            };

            var orientation = {
                x: message.pose.orientation.x,
                y: message.pose.orientation.y,
                z: message.pose.orientation.z,
                w: message.pose.orientation.w,
            };


            this.pose = pose;
            this.orientation = orientation;
            this.robotPoseUpdated = true;
        });


        // lidar scan listener decl.


        var scanListener = new ROSLIB.Topic({
            ros: this.ros,
            name: '/lrf/scan',
            messageType: 'sensor_msgs/LaserScan'
        });

        scanListener.subscribe((message) => {
            // console.log("lidar subscription started");
            this.lidar.angle_min = message.angle_min;
            this.lidar.angle_max = message.angle_max;

            this.lidar.angle_increment = message.angle_increment;
            this.lidar.range_min = message.range_min;
            this.lidar.range_max = message.range_max;
            this.lidar.ranges = message.ranges;

        });

        this.actionClient = new ROSLIB.ActionClient({
            ros: this.ros,
            serverName: '/workerbee_navigation/moveto',
            actionName: 'workerbee_navigation/MoveToAction'
        });



    }


    establishStomp = () => {
        this.smartHousingExchange = "smart_housing";
        this.stomp = new Wstomp();

        this.stomp.connect().then((rabbitClient) => {
            console.log("connection established to rabbit mq");
            this.sendSystemStatus();
            this.doSequence();
        }).catch((e) => {
            console.error("stomp error : ", e);
        });
    }

    sendSystemStatus = () => {
        let status =
        {
            "enabled": true,
            "robots": [
                {
                    "id": 1,
                    "enabled": true
                }
            ]
        }

        this.stomp.send(this.smartHousingExchange, 'status', JSON.stringify(status));
        console.log("system status published");

        // let message = new Paho.MQTT.Message(JSON.stringify(status));
        // message.destinationName = "test";

        // var smessage = JSON.stringify(status);
        // this.mqtt.send(message);
        // console.log("mqtt sent");
        // this.rabbit_client.send(stomp_url, { priority: 9 }, smessage);
    }

    doSequence = async () => {
        let residentStatus = await this.requestResidentStatus();
        console.log("재실정보 파악 : ", residentStatus);

        console.log("배송시작 알림");
        this.notiDeliveryStart();

        console.log("배송시작 알림");
        let gateResponse = await this.notiGateReached();
        console.log("공동현관문 열림 : ", gateResponse);



        /*
         listen elevator event before calling elevator
         */
        let elevatorDoorOpenWait = this.waitElevatorDoorOpen();
        let elevatorArrival = this.waitElevatorArriaval();

        let callElevatorResult = await this.callElevator();
        console.log("엘리베이터 호출 결과도착함 : ", callElevatorResult);

        elevatorArrival.then((e) => {
            console.log("엘리베이터 도착함 : ", e);
        });

        elevatorDoorOpenWait.then((e) => {
            console.log("엘리베이터 문 열림", e);
        });


        /*
         listen elevator floor event before calling elevator
         */
        let floorPromise = this.waitElevatorFloor(3);
        let boardState = await this.sendElevatorBoardOn(1);
        console.log("탑승상태 : ", boardState);

        let targetFloorResult = await floorPromise;
        // floorPromise.then((message) => {
        //     console.log("목적지 층에 엘리베이터 도착함 : ", message);
        // });
        let deliveryArriaval = await this.sendDeliveryArrival();
        console.log("배송체가 현관문앞에 있음을 알림", deliveryArriaval);

        /*
        * 재실자 인증코드 요청 결과 기다리기
        */
        let codeResponsePromise = this.waitOrderResponse();
        /*
        * 재실자 인증코드 요청
        */
        await this.sendOrderCodeRequest();

        /* 재실자 인증코드 결과 받음*/
        let codeResult = await codeResponsePromise;
        console.log("재실자인증코드 받음 :", codeResult);

        /* 배송완료요청결과 기다리기 */
        let deliveryFinishResponse = this.waitDeliveryFinishResponse();

        /* 배송완료승인 요청 */
        this.sendDeliveryFinishRequest();

        deliveryFinishResponse = await deliveryFinishResponse;
        console.log("배송완료요청승인결과", deliveryFinishResponse);

    }

    requestResidentStatus = () => {

        let promise = new Promise((resolve, reject) => {
            this.stomp.subscribe(this.smartHousingExchange, 'resident.state.response', (message) => {
                resolve(message);
            });
            let msg = {
                id: this.uuidv4(),
                block: 1,
                building: 204,
                unit: 1102,
                type: 0
            }

            this.stomp.send(this.smartHousingExchange, "resident.state", msg);
            console.log("resident.state requested :", msg);
        });

        return promise;

        // promise.then((message)=> {
        //     console.log("resident.state.response rechead : ", message);
        //     callback(message);
        // });

    }


    notiDeliveryStart = () => {
        let message = {
            id: this.uuidv4(),
            block: 1,
            buliding: 204,
            unit: 1102,
            code: 1234,
            type: 0
        }

        this.stomp.send(this.smartHousingExchange, "delivery.start", message);
        console.log("배송시작 알림 : delivery.start notififed");

    }

    /*
     * 현관문 도착 알림
    */
    notiGateReached = () => {
        let message = {
            block: 1,
            buliding: 204,
            unit: 1102,
            floor: 1,
            state: 0
        }

        let promise = new Promise((resolve, reject) => {
            this.stomp.subscribe(this.smartHousingExchange, 'door.open', (message) => {
                resolve(message);
            });
        });
        this.stomp.send(this.smartHousingExchange, "move.state", message);
        console.log("공동현관문 도착 알림");

        return promise;
    }


    callElevator = () => {
        let message = {
            id: this.uuidv4(),
            block: 1,
            buliding: 204,
            unit: 1102,
            type: 0,
            origin: 1,
            destination: 5
        }

        let promise = new Promise((resolve, reject) => {
            this.stomp.subscribe(this.smartHousingExchange, 'elevator.call.response', (message) => {
                // console.log("엘리베이터 호출 결과 도착. 리솔브드");
                resolve(message);
            });
        });
        console.log("엘리베이터 호출  ");
        this.stomp.send(this.smartHousingExchange, "elevator.call", message);
        return promise;
    }

    waitElevatorArriaval = () => {
        return new Promise((resolve, reject) => {
            this.stomp.subscribe(this.smartHousingExchange, 'elevator.arrival', (message) => {
                // console.log("엘리베이터 도착 함");
                resolve(message);
            });
        });

    }

    waitElevatorDoorOpen = () => {
        return new Promise((resolve, reject) => {
            this.stomp.subscribe(this.smartHousingExchange, 'elevator.door.state', (message) => {
                resolve(message);
            });
        });
    }

    sendElevatorBoardOn = (state) => {

        return new Promise((resolve, reject) => {
            let message = {
                ev_id: "ev-204-1",
                state: state    //배송체 탑승 상태 (0: 탑승 대기, 1: 탑승 완료, 2: 탑승     취소, 3: 하차 대기, 4: 하차       완료, 5: 하차 취소)
            }

            console.log("엘리베이터 탑승상태  알려줌", state);
            this.stomp.send(this.smartHousingExchange, "board.state", message);
            resolve(true);
        });

    }

    // 목적지 층에 엘리베이터 도착 하기를 기다림.
    waitElevatorFloor = (targetFloor) => {
        return new Promise((resolve, reject) => {
            this.stomp.subscribe(this.smartHousingExchange, 'elevator.floor', (message) => {
                let jmsg = JSON.parse(message.body);
                console.log("엘리베이터 현재 층 : ", jmsg.floor, " 목적지층:", targetFloor);
                if (Number(jmsg.floor) == targetFloor) {
                    console.log("here we are");

                    resolve(message.body);
                }
            });
        });
    }

    // 배송체가 현관문 앞에 도착했음을 알림 
    sendDeliveryArrival = () => {
        return new Promise((resolve, reject) => {

            let message = {
                block: 1,
                buliding: 204,
                unit: 1102
            }

            this.stomp.send(this.smartHousingExchange, "delivery.arrival", message);
            resolve(true);
        });
    }


    /*
    * 재실자 인증코드 요청하기
    */
    sendOrderCodeRequest = () => {
        return new Promise((resolve, reject) => {
            let message = {
                id: this.uuidv4(),
                block: 1,
                buliding: 204,
                unit: 1102,
                result: 0
            }
            this.stomp.send(this.smartHousingExchange, "delivery.code.request", message);
            resolve(true);
        });
    }

    /*
    * 재실자 인증코드 결과 기다리기
    */
    waitOrderResponse = (targetFloor) => {
        return new Promise((resolve, reject) => {
            this.stomp.subscribe(this.smartHousingExchange, 'delivery.code.response', (message) => {
                resolve(message);
            });
        });
    }

    /*
    * 배송종료승인요청하기
    */
    sendDeliveryFinishRequest = () => {

        let message = {
            id: "fc0cc167-23a2-4986-abfe-ba01006e4a0b",
            block: 1,
            buliding: 204,
            unit: 1102,
            type: 0
        }

        this.stomp.send(this.smartHousingExchange, "delivery.finish", message);
    }

    /*
    * 배송종료승인기다리기
    */
    waitDeliveryFinishResponse = () => {
        return new Promise((resolve, reject) => {
            this.stomp.subscribe(this.smartHousingExchange, 'delivery.finish.response', (message) => {
                resolve(message);
            });
        });
    }

    uuidv4 = () => {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }


    moveTo = (pose) => {
        pose = this.makePoseFrame(pose);
        this.goal = new ROSLIB.Goal({
            actionClient: this.actionClient,
            goalMessage: {
                goal: pose,
                speed: 1.5,
                patience_timeout: 100,
                disable_obstacle_avoidance: false
            }
        });

        this.goal.on('feedback', function (feedback) {
            var code = feedback.state.action_state.code;
            var details = feedback.state.action_state.details;
            console.log("code: ", code, "detail: ", details);

            if (code != 0) {
                console.log("faults: ", feedback.state.action_state.faults);
                console.error("error_conditions: ", feedback.state.action_state.error_conditions);
                console.error("driver_state_code: ", feedback.state.driver_state.code, " driver_state_details: ", feedback.state.driver_state.details);
            }
        });


        let promise = new Promise((resolve, reject) => {
            this.goal.on('result', function (result) {
                resolve(result);
                console.log("result code : ", result.state.action_state.code, " details: ", result.state.action_state.details);
            });
        });

        this.goal.send();
        return promise;
    }


    cancleGoal = () => {
        console.log("cancle goal");

        if (this.goal != undefined || this.goal != null) {
            this.goal.cancel();
        }
    }


    makePoseFrame = (pose) => {
        var targetPose = {
            header: {
                stamp: {
                    secs: 0,
                    nsecs: 0
                },
                frame_id: "map"
            },
            pose: pose.pose
        }

        return targetPose;
    }

};

