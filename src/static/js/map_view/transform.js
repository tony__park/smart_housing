/*
        dependancy : THREE.js
    */
class Transform {
    constructor(mapCenterCoord, resolution) {
        this.mapCenterCoord = mapCenterCoord;
        this.resolution = resolution;
    }

    
    quat2Euler = (orientation) => {
        var euler = new THREE.Euler();
        var quat = new THREE.Quaternion(orientation.x, orientation.y, orientation.z, orientation.w, "XYZ");
        euler = euler.setFromQuaternion(quat, 'XYZ');
        var angle = -euler._z + Math.PI / 2;
        return angle;
    }

    robotposeToCanvas(pose, orientation) {
        var rdx = pose.x;
        var rdy = pose.y;

        //
        // transform (robot-coord to pixel coord)
        //
        var rpx = rdx / this.resolution;
        var rpy = rdy / this.resolution;

        //
        // applying pixel coord
        //
        rpx = this.mapCenterCoord.x + rpx;
        rpy = this.mapCenterCoord.y - rpy;


        //
        // transform (robot-coord to canvas coord)
        //
        //
        var euler = new THREE.Euler();
        var quat = new THREE.Quaternion(orientation.x, orientation.y, orientation.z, orientation.w, "XYZ");
        euler = euler.setFromQuaternion(quat, 'XYZ');
        var angle = -euler._z + Math.PI / 2;

        // var angle = -orientation + Math.PI / 2;


        return {
            x: rpx,
            y: rpy,
            angle: angle
        }
    }

    canvasToRobotPose(pose, orientation) {
        //
        // coord transform
        //
        var cnvx = pose.x;
        var cnvy = pose.y;

        var rpx = cnvx - this.mapCenterCoord.x;
        var rpy = cnvy - this.mapCenterCoord.y;
        //
        // flip y axis
        //
        rpy = (-1) * rpy;

        let rdx = rpx * this.resolution;
        let rdy = rpy * this.resolution;

        return {
            x: rdx,
            y: rdy,
            angle: 0.0
        };
    }
};


