
class Map {
    constructor(ctx, imageUrl = "") {

        this.poiImage = new Image();
        this.poiImageUrl = "/static/image/icons/img-profile-addy_4.png";
        this.poiImage.onLoad = function() {
            console.log("PoiImage loaded");
        }
        this.poiImage.src = this.poiImageUrl;

        // const DEFAULT_MAP_IMAGE_URL = "static/image/map/wr4f.png";
        // const DEFAULT_MAP_IMAGE_URL = "static/image/map/wr1f.png";
        const DEFAULT_MAP_IMAGE_URL = "/static/image/map/map.png";

        this.ctx = ctx;
        if (imageUrl == "") {
            imageUrl = DEFAULT_MAP_IMAGE_URL;
        }
        this.imageUrl = imageUrl;
        this.image = new Image();

        this.onLoad = null;



    }

    draw() {
        // this.ctx.clearRect(0, 0, this.image.width, this.image.height); 
        this.ctx.drawImage(this.image, 0, 0);
    }

    loadImage() {
        if (this.onLoad != null) {
            this.image.onload = this.onLoad;
        }
        this.image.src = this.imageUrl;
    }

    loadPOIImage = () => {
        var promise = new Promise(function (resolve, reject) {
            if (this.poiImageLoaded) {
                resolve(this.poiImage);
            }
            this.poiImage.onLoad = function () {
                console.log("POI  Image resolved");
                resolve(this.poiImage);
            }

            this.poiImage.src = this.poiImageUrl;

        }.bind(this));

        return promise;
    }

    drawPOIs = (pois, transform) => {
        // this.loadPOIImage().then(function (poiImage) {
        //     var poi_array = Object.values(pois);
        //     poi_array.forEach(function (item, index) {
        //         var pose = item.pose.position;
        //         var orientation = item.pose.orientation;
        //         console.log(pose.x, ", ", pose.y);
        //         this.drawPOI(pose, orientation, this.transform).bind(this);

        //     }.bind(this));
        // }).catch(function (e) {
        //     console.error(e);
        // });


        // console.log(pois);

        var poi_array = Object.values(pois);
        poi_array.forEach((item, index) => {
            var pose = item.pose.position;
            var orientation = item.pose.orientation;
            this.drawPOI(item.pose.position, item.pose.orientation, transform, item.name);
        });

    }


    drawPOI = (pose, orientation, transform, name) => {
        // ar pose = item.pose;
        // orientation = orientation;
        var euler_angle = transform.quat2Euler(orientation);
        var transformed = transform.robotposeToCanvas(pose, euler_angle);

        this.ctx.save();
        this.ctx.translate(transformed.x, transformed.y);
        var left = -10;
        var top = -10;
        var width = 20;
        var height = 20;
        this.ctx.font = "20px malgun gothic bold";
        this.ctx.fillStyle = "rgba(255, 0, 255, 1)";
        this.ctx.fillText(name, left, top);
        this.ctx.rotate(transformed.angle);
        this.ctx.drawImage(this.poiImage, -10/*-radius*/, -10/*-radius*/, 10 *2 /*radius * 2*/, 10 *2 /*radius * 2*/);
        this.ctx.fillStyle = "rgba(0, 0, 255, 1)";
        this.ctx.strokeRect(left, top, width, height);



        this.ctx.restore();
    }
};