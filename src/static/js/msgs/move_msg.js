'use strict';

class MoveMessage {
    constructor(settings) {
        this.settings = settings || {}
        this.message = {
            "header": {
                "messageName": "move/request",
                "transactionId": "202109241122330001",
                "senderName": "ACS",
                "receiverName": settings.receiverName /* "wmr001" */
            },
            "body": {
                "deviceId": settings.deviceId,
                "taskId": "DLV-0001",
                "functionId": "move/0001",
                "goalPoi": {
                    "id": "1f_wait_1_power_4",
                    "type": 1,
                    "pose": {
                        "x": settings.pose.x,
                        "y": settings.pose.y,
                        "theta": settings.pose.theta,
                        "zone": 1
                    }
                }
            },
            "return": {
                "returnCode": 0,
                "returnMessage": null
            }
        };
    }
}