const { v4: uuidv4 } = require('uuid');
var queue = require('block-queue');

class SHBroker {
    constructor() {
        this.config = require("../server_config/config.json");

        this.mqtt = require('mqtt');
        this.options = {
            host: this.config.mqtt.host,
            port: this.config.mqtt.port,
            username: this.config.mqtt.Username,
            password: this.config.mqtt.Passwd,
            rejectUnauthorized: false
        }


        this.rcsId = this.config.robot.rcsId;
        this.robotId = this.config.robot.robotId;
        this.client = null;

        console.log("constructor");

    }



    connect = () => {
        this.client = this.mqtt.connect(this.options);
        return new Promise((resolve, reject) => {
            this.client.on("connect", () => {

                //
                // subscribe resident check
                //
                //
                //const residentSubsUrl = `/WRrobot/${this.rcsId}/robot/${this.robotId}/resident/state/response`;

                resolve(this.client);
            });

            this.client.on("error", (error) => {
                console.error(error);
                reject(error);
            });
        });
    }


    sendSystemStatus = (status) => {
        status = {
            "enabled": "true",
            "robots": [
                {
                    "id": this.rcsId,
                    "enabled": true
                }
            ]
        }

        let topic = `/WRrobot/${this.rcsId}/system/status`;
        this.client.publish(topic, JSON.stringify(status, null, 2), {}, () => {
            console.log("topic published");
        });

    }

    sendLWT = () => {
        console.error("no operation");
    }

    requestResidentInfo = (request) => {
        let req = request || {}

        return new Promise((resolve, reject) => {
            let uuid = uuidv4();
            // const residentSubsUrl = `/WRrobot/#`;
            const residentSubsUrl = `/WRrobot/${this.rcsId}/robot/${this.robotId}/resident/state/response`;
            this.client.subscribe(residentSubsUrl, () => {
                console.log(`재실정보기다리기 시작 '${residentSubsUrl}'`);
            });

            this.client.on("message", (topic, payload) => {
                let j = JSON.parse(payload);
                if (j.id === uuid) {
                    this.client.unsubscribe(residentSubsUrl);
                }


                resolve(
                    {
                        topic: topic,
                        payload: j
                    }
                );
            });


            let msg = {
                "id": uuid,
                "block": 1,
                "building": 204,
                "unit": 1102,
                "type": 0
            }
            let topicUrl = `/WRrobot/${this.rcsId}/robot/${this.robotId}/resident/state`;
            this.client.publish(topicUrl, JSON.stringify(msg, null, 2));

            console.log("재실정보 요청 published");

        });
    }


    deliveryStart = () => {

        return new Promise((resolve, reject) => {
            let uuid = uuidv4();
            let surl = `/WRrobot/${this.rcsId}/robot/${this.robotId}/delivery/start/response`;
            this.client.subscribe(surl, () => {
                console.log(`Subscribe to topic '${surl}'`);
            });

            this.client.on("message", (topic, payload) => {
                let j = JSON.parse(payload);
                if (j.id === uuid) {
                    this.client.unsubscribe(surl);
                    resolve(
                        {
                            topic: topic,
                            payload: j
                        }
                    );
                }

            });

            let url = `/WRrobot/${this.rcsId}/robot/${this.robotId}/delivery/start`;
            let message = {
                id: uuid,
                block: 1,
                buliding: 204,
                unit: 1102,
                code: 1234,
                type: 0
            }
            this.client.publish(url, JSON.stringify(message, null, 2));

        });

    }


    notiFrontDoorState = (state) => {


        return new Promise((resolve, reject) => {
            let url = `/WRrobot/${this.rcsId}/robot/${this.robotId}/door/open`;
            this.client.subscribe(url, () => {
                console.log(`공동현관문 개방 기다리기 시작 : ${url}`);
            });

            this.client.on("message", (topic, payload) => {
                console.log("공동현관문 개방됨");
                let j = JSON.parse(payload);
                this.client.unsubscribe(url);
                resolve(
                    {
                        topic: topic,
                        payload: j
                    }
                );
            });

            let uuid = uuidv4();
            let pubURL = `/WRrobot/{${this.rcsId}}/robot/${this.robotId}/door/move/state`;
            let message = {
                block: 1,
                buliding: 204,
                unit: 1102,
                floor: 1,   
                state: 0
            }
    
            this.client.publish(pubURL, JSON.stringify(message, null, 2), {}, () => {
                console.log("공동현관문 도착 알림 published");
            });

        });

    }

    callElevator = () => {
        let message = {
            id: "e23a4a85-c849-48db-ab5b-a55794a567f7",
            block: 1,
            buliding: 204,
            unit: 1102,
            type: 0,
            origin: 1,
            destination: 5
        }

        return new Promise((resolve, reject) => {
            let pubUrl = `/WRrobot/${this.rcsId}/robot/${this.robotId}/elevator/call`;
            let subUrl = `/WRrobot/${this.rcsId}/robot/${this.robotId}/elevator/call/response`;
            this.client.subscribe(subUrl, () => {
                console.log(`엘리베이터 호출 결과 기다리기 시작 : ${subUrl}`);
            });

            this.client.on("message", (topic, payload) => {
                console.log("엘리베이터 호출 응답 도착");
                let j = JSON.parse(payload);
                this.client.unsubscribe(subUrl);
                resolve(
                    {
                        topic: topic,
                        payload: j
                    }
                );
            });
        
            message.id = uuidv4();  
            this.client.publish(pubUrl, JSON.stringify(message, null, 2), {}, () => {
                console.log("엘리베이터 호출 published");
            });
        
        });
    }

    waitElevatorDoorState = () => {

        return new Promise((resolve, reject) => {
            let subUrl = `/WRrobot/${this.rcsId}/robot/${this.robotId}/elevator/arrival`;
            // let subUrl = `/WRrobot/#`;
            this.client.subscribe(subUrl, () => {
                console.log(`엘리베이터 목적지 층 도착 기다리기 시작 : ${subUrl}`);
            });
    
            this.client.on("message", (topic, payload) => {
                console.log("엘리베이터 목적지 층 도착 알림 OK");
                let j = JSON.parse(payload);
                this.client.unsubscribe(subUrl);
                resolve(
                    {
                        topic: topic,
                        payload: j
                    }
                );
            });
        });
    }
}



module.exports = SHBroker;