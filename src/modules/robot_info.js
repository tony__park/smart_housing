
'use strict';

const Rabbit = require("./rabbit.js");

class RobotInfo {

    constructor(settings) {
        this.name = settings.name;


        this.host = settings.host;
        this.user = settings.user;
        this.pass = settings.pass;


        // this.pExchanageInfo = this.makeProducerExchangeNameFromRobotName(this.name);
        // this.cExchangeInfo = this.makeConsumerExchangeNameFromRobotName(this.name);
        
        // this.cRabbit = this.createConsumerRabbit(this.cExchangeInfo);
        // this.pRabbit = this.createProducerRabbit(this.pExchanageInfo);

        this.consumerX = "wmr2acs.wmr" + this.name;
        this.consumerRK = "xwmr.wmr" + this.name;
        this.producerX = "acs2wmr.wmr" + this.name;
        this.producerRK = "xacs.wmr" + this.name;

        this.producerRabbit = null;
        this.consumerRabbit = null;
        // this.listener_callback = listener_callback;
        this.createRabbits();

    }

    createRabbits = (settins) => {
        var rabbitSetting = {
            'host' : this.host,
            'user' : this.user,
            'pass' : this.pass
        }
        /* assert producer rabbit*/
        this.producerRabbit = new Rabbit(rabbitSetting);
        this.producerRabbit.connect().then((connection) => {
            return this.producerRabbit.createChannel(connection);
        }).then((channel)=> {
            var x =this.producerRabbit.createExchange(channel, this.producerX, this.producerRK);
            console.info("[*] " + this.name +"/ exchange / " + this.producerX +"/ " + this.producerRK +"/ crated"); 
            return this.producerRabbit.createExchange(channel, this.producerX, this.producerRK);

        }).catch((e)=>{
            console.error("RobotInfo.js : cannot create exchange[" + e +"]");
        });


        this.consumerRabbit = new Rabbit(rabbitSetting);
        this.consumerRabbit.connect().then((connection) => {
            return this.consumerRabbit.createChannel(connection);
        }).then((channel)=> {
            return this.consumerRabbit.createExchange(channel, this.consumerX, this.consumerRK);
        }).catch((e)=>{
            console.error("RobotInfo.js : cannot create exchange[" + e +"]");
        });
        
    }
}


module.exports = RobotInfo;