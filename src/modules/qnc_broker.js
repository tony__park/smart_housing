
class QncBroker {
    constructor(app, config) {
        this.app = app;
        this.config = config;
        this.robots = [];


        this.makeRobots();
    }


    makeRobots = () => {
        const RobotInfo = require('./robot_info.js');
        var cfg_robot_names = this.config.robot_names;
        var settings = {}
        cfg_robot_names.forEach(function (name, index) {
            settings = {}
            settings.name = name;
            settings.host = this.config.rabbitmq.host;
            settings.user = this.config.rabbitmq.user;
            settings.pass = this.config.rabbitmq.pass;

            var robotInfo = new RobotInfo(settings);
            this.robots.push(robotInfo);



            // var pxInfo = this.makeProducerExchangeNameFromRobotName(name);
            // var cxInfo = this.makeConsumerExchangeNameFromRobotName(name);
            // var settings = {
            //     robot_name : name, 
            //     rabbit : this.rabbit,
            //     pExchange : pxInfo.exchange_name, 
            //     pRoutingKey : pxInfo.routing_key,
            //     cExchange : cxInfo.exchange_name, 
            //     cRoutingKey : cxInfo.cRoutingKey,
            //     listenerCallback : this.robotListener
            // };
            // var robotInfo = new RobotInfo(settings);
            // robotInfo.subscribe();
            // this.robots.push(robotInfo);
        }.bind(this));
    }

    robotListener = (robotName, exchangeName, routingKey, msg) => {

        console.log("qncBroker:robotListener", robotName, exchangeName, routingKey, msg.content.toString());

    }

}

module.exports = QncBroker;