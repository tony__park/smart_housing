

class WBroker {
    constructor(mqtt_broker, rabbit_broker) {

        this.config = require("../server_config/config.json");

        const SHBroker = require("./sh_broker");
        const Rabbit = require("./rabbit.js");
        this.shb = new SHBroker();
        this.rabbit = new Rabbit();


        this.amqp = require('amqplib/callback_api');
        this.amqURL = `amqp://${this.config.rabbitmq.host}`;
        this.exchange = "robot";
        this.smartHousingExchange = "smart_housing";
        this.channel = null;

        let promise = new Promise((resolve, reject) => {
            //
            // create rabbit connection
            //
            //
            this.amqp.connect(this.amqURL, (error0, connection) => {
                if (error0) {
                    reject(error0);
                }
                resolve(connection);
            });
        }).then((connection) => {
            //
            // create channel
            //
            //
            return new Promise((resolve, reject) => {
                connection.createChannel((error, channel) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(channel);
                    }

                });
            });
        }).then((channel) => {
            //
            // create exchange
            //
            //
            console.log("amq channel created. wait exchange message");
            this.channel = channel;

            channel.assertExchange(this.smartHousingExchange, 'direct', { durable: false });
            channel.assertQueue('', { exclusive: false }, (error2, queue) => {
                if (error2) {
                    reject(error2);
                }
                channel.bindQueue(queue.queue, this.smartHousingExchange, "status");
                channel.consume(queue.queue, (msg) => {

                    let content = msg.content.toString();
                    let req = JSON.parse(content);
                    console.log("system status notification");
                    channel.unbindQueue(queue.queue, this.smartHousingExchange);
                }, { noAck: true });
            });


            /*
                재실상태 파악 요청
            */
            channel.assertQueue('', { exclusive: false }, (error2, queue) => {
                if (error2) {
                    reject(error2);
                }
                channel.bindQueue(queue.queue, this.smartHousingExchange, "resident.state");
                channel.consume(queue.queue, (msg) => {

                    let content = msg.content;
                    let req = JSON.parse(content);
                    console.log("resident.state reuest received");

                    let res = req;

                    console.log("res : ", res);
                    res.state = 1; // 재실상황임
                    //
                    // 재실상황 return
                    //
                    //
                    channel.publish(this.smartHousingExchange, 'resident.state.response', Buffer.from(JSON.stringify(res)));
                    channel.unbindQueue(queue.queue, this.smartHousingExchange);

                }, { noAck: true });
            });

            /*
                배송시작
            */
            channel.assertQueue('', { exclusive: false }, (error2, queue) => {
                if (error2) {
                    reject(error2);
                }
                channel.bindQueue(queue.queue, this.smartHousingExchange, "delivery.start");
                channel.consume(queue.queue, (msg) => {

                    let content = msg.content;
                    let req = JSON.parse(content);
                    console.log("delivery.staart notified");


                    let res = req;
                    res.result = 0; //(0: 배송 수락, 1: 배송 거부,  2: reserve)

                    //
                    // 배송수락여부 response
                    //
                    //
                    channel.publish(this.smartHousingExchange, 'delivery.start.response', Buffer.from(JSON.stringify(res)));

                }, { noAck: true });
            });

            /*
                현관문 도착
            */
            channel.assertQueue('', { exclusive: false }, (error2, queue) => {
                if (error2) {
                    reject(error2);
                }
                channel.bindQueue(queue.queue, this.smartHousingExchange, "move.state");
                channel.consume(queue.queue, (msg) => {

                    let content = msg.content.toString();
                    let req = JSON.parse(content);
                    console.log("move.state notified");


                    /* 현관문 개방 알림 */
                    let res = req;
                    res.state = 0; // 0: 현관문 열림  1:현관문 닫힘 2 : reserved
                    channel.publish(this.smartHousingExchange, 'door.open', Buffer.from(JSON.stringify(res)));


                }, { noAck: true });
            });


            /*
                엘리베이터 호출
            */
            channel.assertQueue('', { exclusive: false }, (error2, queue) => {
                if (error2) {
                    reject(error2);
                }
                channel.bindQueue(queue.queue, this.smartHousingExchange, "elevator.call");
                channel.consume(queue.queue, (msg) => {

                    console.log("elevator.call requested");
                    let content = msg.content.toString();
                    let req = JSON.parse(content);

                    let res = {
                        id: req.id,
                        result: 0, // 결과코드
                        ev_id: "ev-204-1",
                        origin: 1, //출발층
                        destination: req.destination //도착층
                    }

                    /* 엘리베이터 호출 메세지 수신 알림 */
                    channel.publish(this.smartHousingExchange, 'elevator.call.response', Buffer.from(JSON.stringify(res)));

                    /*
                     * 엘리베이터 도착 알림 
                    */
                    channel.publish(this.smartHousingExchange, 'elevator.arrival', Buffer.from(JSON.stringify(res)));
                    console.log("엘리베이터 도착했다고, 브라우저에게 알려줌");

                    /*
                     * 엘리베이터 문열림 알림
                    */
                    let doorState = {
                        ev_id: "ev-204-1",
                        state: 0 // 0: 열림, 1: 열림 중, 2: 닫힘, 3: 닫힘 중
                    }
                    channel.publish(this.smartHousingExchange, 'elevator.door.state', Buffer.from(JSON.stringify(doorState)));
                    console.log("엘리베이터 문 열렸음을, 브라우저에게 알려줌");

                }, { noAck: true });
            });



            /*
                엘리베이터 탑승상태
            */

            channel.assertQueue('', { exclusive: false }, (error2, queue) => {
                if (error2) {
                    reject(error2);
                }
                channel.bindQueue(queue.queue, this.smartHousingExchange, "board.state");
                channel.consume(queue.queue, (msg) => {

                    let content = msg.content.toString();
                    let req = JSON.parse(content);
                    console.log("elevator.call requested");


                    /*
                    * 탑승인 경우, 현재 층 전송
                    * 1층 -> 3층 
                    */
                    let res = {
                        ev_id: "ev-204-1",
                        floor: 1
                    }
                    channel.publish(this.smartHousingExchange, 'elevator.floor', Buffer.from(JSON.stringify(res)));

                    res.floor = 2;
                    channel.publish(this.smartHousingExchange, 'elevator.floor', Buffer.from(JSON.stringify(res)));

                    res.floor = 3;
                    channel.publish(this.smartHousingExchange, 'elevator.floor', Buffer.from(JSON.stringify(res)));

                    console.log("엘리베이터 현재층 알려줌 끝!!");


                }, { noAck: true });
            });

            /*
                무인 배송체가 세대 현관문 앞에 도착했음을 알림
            */
            channel.assertQueue('', { exclusive: false }, (error2, queue) => {
                if (error2) {
                    reject(error2);
                }
                channel.bindQueue(queue.queue, this.smartHousingExchange, "delivery.arrival");
                channel.consume(queue.queue, (msg) => {

                    let content = msg.content.toString();
                    let req = JSON.parse(content);
                    console.log("elevator.call requested");
                }, { noAck: true });
            });


            //
            // 무인 배송체 수납장 오픈을 위해 재실자 인증 코드 전송
            //
            channel.assertQueue('', { exclusive: false }, (error2, queue) => {
                if (error2) {
                    reject(error2);
                }
                channel.bindQueue(queue.queue, this.smartHousingExchange, "delivery.code.request");
                channel.consume(queue.queue, (msg) => {

                    let content = msg.content.toString();
                    let req = JSON.parse(content);
                    console.log("elevator.call requested");

                    let res = {
                        id: "fc0cc167-23a2-4986-abfe-ba01006e4a0b",
                        block: 1,
                        buliding: 204,
                        unit: 1102,
                    }

                    // 제품 인증 코드 결과
                    channel.publish(this.smartHousingExchange, 'delivery.code.response', Buffer.from(JSON.stringify(res)));


                }, { noAck: true });
            });

            //
            // 재실자 인증 코드 Response Topic : 재실자 인증 코드 Topic 에 대한 처리 결과를 반환
            //
            channel.assertQueue('', { exclusive: false }, (error2, queue) => {
                if (error2) {
                    reject(error2);
                }
                channel.bindQueue(queue.queue, this.smartHousingExchange, "delivery.code.response");
                channel.consume(queue.queue, (msg) => {

                    let content = msg.content.toString();
                    let req = JSON.parse(content);
                    console.log("elevator.call requested");
                }, { noAck: true });
            });


            //
            // 배송완료
            //            
            channel.assertQueue('', { exclusive: false }, (error2, queue) => {
                if (error2) {
                    reject(error2);
                }
                channel.bindQueue(queue.queue, this.smartHousingExchange, "delivery.finish");
                channel.consume(queue.queue, (msg) => {

                    let content = msg.content.toString();
                    let req = JSON.parse(content);
                    console.log("elevator.call requested");
                    let res = {
                        "id": req.id,
                        "block": 1,
                        "buliding": 204,
                        "unit": 1102,
                        "result": 0
                    }
                    channel.publish(this.smartHousingExchange, 'delivery.finish.response', Buffer.from(JSON.stringify(res)));

                }, { noAck: true });
            });



        }).catch((e) => {
            console.log("rabbitmq connection error", e);
        });
    }


    requestResidentInfo = () => {
        console.log("w_broker : requestResidentInfo");
        this.shb.requestResidentInfo().then((message) => {
            message = message.payload;
        });
    }




}

module.exports = WBroker;