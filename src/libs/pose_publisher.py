from publisher import Publisher

pub = Publisher(exchange_name="wmr2acs001", exchange_type='direct', routing_key='xwmr')

message = {
    'header' : {
        'message_name' : 'robot_status/notification'
    },
    'body' :  {
        'pose' : {
            'x' : 100,
            'y' : 200,
            'theta' : 300
        }
    }
}

import time
for i in range(10) :
    pub.publish(message)
    print "published"
    time.sleep(1)
