# -*- coding: utf-8 -*-
#!/usr/bin/env python
import pika
import json


from subscriber import Subscriber
from publisher import Publisher


class Requester :
    def __init__(self, routing_key, on_message_callback) :
        sub_routing_key = routing_key + "." + "response"
        pub_routing_key = routing_key + "." + "request"

        print(pub_routing_key + ":" + sub_routing_key)

        self.subscriber = Subscriber(exchange_name="r1.exchange", exchange_type="direct", routing_key=sub_routing_key, on_message=on_message_callback)
        self.publisher = Publisher(exchange_name="r1.exchange", exchange_type="direct", routing_key=pub_routing_key)
        self.subscriber.subscribe(async_flag = True)

    def on_response(self, ch, method, properties, body) :
        print("on resonse")
        response = json.dumps(body, ensure_ascii = False)
        print(response)

    def request(self, data) :
        self.publisher.publish(data)
        print ("request published")

if __name__ == "__main__" :
    r = Requester("poi_list", None)

    for i in range(10) :
        r.request({"a" : "a"})

    import time
    time.sleep(30)
