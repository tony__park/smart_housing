# -*- coding: utf-8 -*-
#!/usr/bin/env python
import pika
import json
import threading


class RPC_Server :
    def __init__(self, qname): 
        self.q_name = qname
        self.callback_proc = self.default_callback_proc

        self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
        self.channel = self.connection.channel()
        result = self.channel.queue_declare(queue=self.q_name)

    def setcallback_proc(self, callback_proc) :
        print("callback setting")
        self.callback_proc = callback_proc


    def default_callback_proc(self, message) :
        print("default callback proc executed")
        return {}


    def on_request(self, ch, method, props, body) :

        print("on request")

        body = {
            "response" : "nothing"
        }
        body = json.dumps(body)
        self.respond(ch, method, props, body)


    def respond(self, ch, method, props, body) :

        '''
            body : default json format
        '''
        body = json.dumps(body)
        ch.basic_publish(exchange='',
                        routing_key=props.reply_to,
                        properties=pika.BasicProperties(correlation_id =  props.correlation_id),
                        body=body)
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def start(self) :
        self.channel.basic_qos(prefetch_count=1)
        self.channel.basic_consume(queue=self.q_name, on_message_callback=self.callback_proc)
        print(" [x] Awaiting RPC requests : [" + self.q_name +"]")
        self.channel.start_consuming()


    def start_async(self) :
        
        def thread_proc() :
            self.channel.basic_qos(prefetch_count=1)
            self.channel.basic_consume(queue=self.q_name, on_message_callback=self.callback_proc)
            print(" [x] Awaiting RPC requests : [" + self.q_name +"]")
            self.channel.start_consuming()

        self.thread = threading.Thread(target = thread_proc, args=())
        self.thread.setDaemon(True)
        self.thread.start()
        print ("server async thread started")
    
