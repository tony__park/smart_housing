# -*- coding: utf-8 -*-
#!/usr/bin/env python
import pika
import json



class Publisher: 
    def __init__(self, **kwargs) :

            self.x_name = kwargs['exchange_name']
            self.x_type = kwargs['exchange_type']
            self.routing_key = kwargs['routing_key']

            credentials = pika.PlainCredentials(username='eosuser', password='eosuser')
            # self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='172.16.6.83'), credential=credential)
            parameters = pika.ConnectionParameters('192.168.1.9',
                                   5672,
                                   '/',
                                   credentials)
                    
            self.connection = pika.BlockingConnection(parameters)
            self.channel = self.connection.channel()
            self.channel.exchange_declare(exchange=self.x_name, exchange_type=self.x_type)


    def publish(self, data) :
        message = json.dumps(data, ensure_ascii = False)
        self.channel.basic_publish(exchange=self.x_name, routing_key=self.routing_key, body=message)
        # print("publish : " + message)

    def close(self) :
        self.connection.close()


if __name__ == "__main__" :
    p = Publisher(exchange_name="wmr2acs.wmr006", exchange_type="direct", routing_key="xwmr.wmr006")
    import time
    for i in range (10) :
        p.publish({
            "a" : "A",
            "b" : "B",
            "c" : "C"
        })
        time.sleep(1)
        print "published"




