# -*- coding: utf-8 -*-
#!/usr/bin/env python
import pika
import json
import uuid
import logging

class RPC_Client :
    def __init__(self, qname) : 
        self.logger = logging.getLogger('qncmon')
        self.qname=qname
        self.on_response_callback = None
        self.response = None

        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(host='localhost'))


        self.channel = self.connection.channel()

        result = self.channel.queue_declare(queue='', exclusive=True)
        self.callback_queue = result.method.queue

        self.channel.basic_consume(
            queue=self.callback_queue,
            on_message_callback=self.on_response,
            auto_ack=True)


    def on_response(self, ch, method, props, body):
        if self.corr_id == props.correlation_id:
            self.response = body

    def call(self, param) :

        '''
            parameter : default json
        '''

        self.corr_id = str(uuid.uuid4())
        self.channel.basic_publish(
            exchange='',
            routing_key="poi_list.request",
            properties=pika.BasicProperties(
                reply_to=self.callback_queue,
                correlation_id=self.corr_id,
            ),
            body=json.dumps(param)
        )

        while self.response is None:
            self.connection.process_data_events()
            # print("process data event")
            # self.logger.debug("process data event")


        return self.response


def init_logging() :
    # 로그 생성
    logger = logging.getLogger('qncmon')

    # 로그의 출력 기준 설정
    logger.setLevel(logging.DEBUG)

    # log 출력 형식
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # log 출력
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)

    # log를 파일에 출력
    #file_handler = logging.FileHandler('my.log')
    #file_handler.setFormatter(formatter)


    # logger.addHandler(file_handler)

if __name__ == "__main__" :

    init_logging()

    client = RPC_Client("poi_list.request")
    param = {
        'fib_to' : 30
    }

    result = client.call(param)
    print(json.dumps(result))