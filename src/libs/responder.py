# -*- coding: utf-8 -*-
#!/usr/bin/env python
import pika
import json


from subscriber import Subscriber
from publisher import Publisher

class Responder :
    def __init__(self, routing_key, on_message_callback) :
        sub_routing_key = routing_key + "." + "request"
        pub_routing_key = routing_key + "." + "response"

        # print("responder: sub_routing_key : " + sub_routing_key)
        # print("responder: pub_routing_key : " + pub_routing_key)

        self.subscriber = Subscriber(exchange_name="r1.exchange", exchange_type="direct", routing_key=sub_routing_key, on_message=on_message_callback)
        self.publisher = Publisher(exchange_name="r1.exchange", exchange_type="direct", routing_key=pub_routing_key)


    def respond(self, data) :
        self.publisher.publish(data)


    def listen(self, **kwargs) :
        print("responder start listening")
        self.subscriber.subscribe(**kwargs)


if __name__ == "__main__" :
    responder = Responder("poi_list", None)
    responder.listen(async_flag = False)




