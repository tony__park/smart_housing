# -*- coding: utf-8 -*-
#!/usr/bin/env python
import pika
import json
import threading




class Subscriber :
    def __init__(self, **kwargs) :
        try :
            self.xname = kwargs['exchange_name']
            self.xtype = kwargs['exchange_type']
            self.routing_key = kwargs['routing_key']
            self.callback = kwargs['on_message']

            self.connection = pika.BlockingConnection(pika.ConnectionParameters(host='192.168.1.9'))
            self.channel = self.connection.channel()
            self.channel.exchange_declare(exchange=self.xname, exchange_type=self.xtype)
            result = self.channel.queue_declare(queue='', exclusive=True)
            self.queue_name = result.method.queue
            print("create queue name : " + self.queue_name)
            self.channel.queue_bind(exchange=self.xname, queue=self.queue_name, routing_key=self.routing_key)

        except Exception as e :
            print ("exception : " + str(e))

    def subscribe(self, **kwargs) :
        try :
            if  kwargs['async_flag'] == True :
                def thread_proc() :

                    self.channel.basic_consume(queue=self.queue_name, on_message_callback=self.callback, auto_ack=True)
                    self.channel.start_consuming()

                thread = threading.Thread(target = thread_proc, args=())
                thread.setDaemon(True)
                thread.start()
                print("async subscription started")

            else : 
                print ("strting sync subscription")
                self.channel.basic_consume(queue=self.queue_name, on_message_callback=self.callback, auto_ack=True)
                self.channel.start_consuming()
        except KeyError as e :
                print ("strting sync subscription")
                self.channel.basic_consume(queue=self.queue_name, on_message_callback=self.callback, auto_ack=True)
                self.channel.start_consuming()


if __name__ == "__main__" :

    def on_message(ch, method, properties, body) :
        print(" [x] %r:%r" % (method.routing_key, body))

    s = Subscriber(exchange_name="wmr2acs.wmr01", exchange_type="direct", routing_key="xwmr.wmr01", on_message=on_message)
    s.subscribe(async_flag = False)
    import time
    time.sleep(30)


