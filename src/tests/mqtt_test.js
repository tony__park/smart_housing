const mqtt = require('mqtt');
const options  = {
    host: 'localhost',
    port: 1883,
    // protocol: 'mqtts',
    username:"test",
    password:"test",
};

const client = mqtt.connect(options);
console.log("mqtt : " + mqtt.url);

client.on("connect", ()=> {
    console.log("connected :"+ client.connected);
});

client.on("error", (error) => {
    console.log("mqtt error : ",  error);
});


client.publish("/amqp.topic", "hello");