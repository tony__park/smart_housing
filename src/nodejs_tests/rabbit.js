'use strict';

class Rabbit {
    constructor(settings) {
        this.settings = settings || {};
        this.amqp = require('amqplib/callback_api');
        // this.connect.bind(this);
        // console.log(this.amqp);
    }



    subscribeLog = () => {
        var amqpURL = this.makeURL();
        this.amqp.connect(amqpURL, function (error0, connection) {
            // console.log(this.settings);


            if (error0) {
                throw error0;
            }
            connection.createChannel(function (error1, channel) {
                if (error1) {
                    throw error1;
                }

                var exchange = 'logs';
                channel.assertExchange(exchange, 'direct', {
                    durable: false
                });

                channel.assertQueue('', { exclusive: true }, function (error2, q) {
                    if (error2) {
                        throw error2;
                    }

                    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
                    channel.bindQueue(q.queue, exchange, '');



                    channel.consume(q.queue, function (msg) {
                        if (msg.content) {
                            console.log(" [x] %s", msg.content.toString());
                        }
                    }, { noAck: true });
                });
            });
        });
    }

    subscribe = (exchangeName, routingKey) => {
        var amqpURL = this.makeURL();
        var promise = new Promise(function (resolve, reject) {
            this.amqp.connect(amqpURL, function (error0, connection) {
                // console.log(this.settings);

                if (error0) {
                    reject(error0);
                }
                connection.createChannel(function (error1, channel) {
                    if (error1) {
                        reject(error1);
                    }

                    var exchange = 'logs';
                    channel.assertExchange(exchange, 'direct', {
                        durable: true
                    });

                    channel.assertQueue('', { exclusive: true }, function (error2, q) {
                        if (error2) {
                            reject(error2);
                        }
                        channel.bindQueue(q.queue, exchange, '');
                        // resolve(connection, channel, q);
                        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
                        channel.consume(q.queue, function (msg) {
                            if (msg.content) {
                                console.log(" [x] %s", msg.content.toString());
                                callback(msg);

                            }
                        }, { noAck: true });

                    });
                });
            });
        }.bind(this));

        return promise;
    }


    connect = () => {
        var url = this.makeURL();
        var promise = new Promise(function (resolve, reject) {
            this.amqp.connect(url, function (error0, connection) {
                if (error0) {
                    reject(error0);
                }
                resolve(connection);
            });
        }.bind(this));

        return promise;
    }

    createChannel = (connection) => {
        var promise = new Promise(function(resolve, reject) {
            connection.createChannel(
                function (error1, channel) {
                    if (error1) {
                        reject(error1);
                    }

                    console.log("channel resolved");
                    return resolve(channel);
                });
        });
        return promise;
    }

    createExchange = (exchangeName, channel, routingkey) => {
        var promise = new Promise(function (resolve, reject) {

            /* default durable false */
            channel.assertExchange(exchangeName, 'direct', { durable: false });
            channel.assertQueue('', { exclusive: false }, function (error2, queue) {
                if (error2) {
                    reject(error2);
                }


                console.log(' [*] Waiting for ' + exchangeName +":" + routingkey+'. To exit press CTRL+C');
                channel.bindQueue(queue.queue, exchangeName, routingkey);

                resolve({
                    channel : channel,
                    queue : queue
                });
            });
        });
        return promise;
    }

    startConsume = (channel, queue, callback) => {
        channel.consume(queue.queue, function (msg) {
            callback(msg);
        }, { noAck: true });
    }

    makeURL() {
        var amqURL = "amqp://" + this.settings.address;
        return amqURL;
    }

};

var rabbit = new Rabbit({ address: "localhost" });
try {
    // rabbit.subscribe("logs").then(function (connection, channel, queue) {
    //     channel.consume(q.queue, function (msg) {
    //         if (msg.content) {
    //             console.log(" [x] %s", msg.content.toString());
    //         }
    //     }, { noAck: true });
    // });


    rabbit.connect().then(function (connection) {
        return rabbit.createChannel(connection);
    }).then(function(channel){
        return rabbit.createExchange('direct_logs', channel, 'info');
    }).then(function(resolved){

        var channel = resolved.channel;
        var queue = resolved.queue;
        rabbit.startConsume(channel, queue, function(msg) {
            console.log(msg.content.toString());
        });
    });

} catch (error) {
    console.log(error);
}


