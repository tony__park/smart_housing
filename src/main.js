
var config = require('./server_config/config.json');
console.log(JSON.stringify(config));

var express = require('express');

var app = express();
var router = require('./router/router')(app);


let Wbroker = require('./modules/w_broker.js');
let wbroker = new Wbroker();

// console.log({
//     'host' : config.rabbitmq.host,
//     'user' : config.rabbitmq.user,
//     'pass' : config.rabbitmq.pass
// });



// const QncBroker = require('./modules/qnc_broker.js');
// const qncBroker = new QncBroker(app, config);

// qncBroker.makeRobots();


const SHBroker = require("./modules/sh_broker");
const shBroker = new SHBroker();

shBroker.connect().then((client)=>{
    console.log("smart housing broker connected");
    // shBroker.sendSystemStatus();
    


    shBroker.requestResidentInfo().then((resolve) => {
        console.log(`재실정보 : ${JSON.stringify(resolve.payload)}`);
    }).catch((error) => {
        console.error("request resident info error");
    });


    shBroker.deliveryStart().then((res) => {
        console.log(`배송시작결과 : ${JSON.stringify(res.payload)}`);
        return "배송시작";
    }).then((d) => {

        let state = {
            block: 1,
            buliding: 204,
            unit: 1102,
            floor: 1,   
            state: 0
        }

        return shBroker.notiFrontDoorState(0);

        // shBroker.notiFrontDoorState(0/*도착*/).then((d) => {
        //     console.log("공동문 개방 결과 : ", d.payload);

        //     /* 공동문 통과 완료 알림 */
        //     // shBroker.notiFrontDoorState(1);

        //     return "공동현관문 도착";
        // });
    }).then((d)=>{
        console.log("공동문 개방 결과 : ", d.payload);
        return shBroker.callElevator();

    }).then((d) => {
        console.log("엘리베이터 호출 응답 OK");

        return shBroker.waitElevatorDoorState();
    });


}).catch((error) => {
    console.error("mqtt error : " + error);
});






app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
app.use('/static', express.static('static'));

var server = app.listen(3000, function(){
    console.log("Express server has started on port 3000")
});

