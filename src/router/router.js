

module.exports = function(app)
{
   var config = require("../server_config/config.json")

     app.get('/',function(req,res){
        res.render('main.html', {
           config : JSON.stringify(config)
        })
     });

     app.get('/config', function(req, res){

      res.send(JSON.stringify(config));
     });

}






