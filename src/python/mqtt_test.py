# -*- coding: utf-8 -*-
import paho.mqtt.client as mqtt
import json


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("connected OK")
    else:
        print("Bad connection Returned code=", rc)


def on_disconnect(client, userdata, flags, rc=0):
    print(str(rc))


def on_publish(client, userdata, mid):
    print("In on_pub callback mid= ", mid)


# 새로운 클라이언트 생성
client = mqtt.Client()
host = "203.233.111.55"
port = 5555
user = "WRrobot"
passwd = "0372"
client.username_pw_set(user, passwd)
# 콜백 함수 설정 on_connect(브로커에 접속), on_disconnect(브로커에 접속중료), on_publish(메세지 발행)
client.on_connect = on_connect
client.on_disconnect = on_disconnect
client.on_publish = on_publish
# address : localhost, port: 1883 에 연결
client.connect(host=host, port=port)
client.loop_start()
import time
time.sleep(10)
# common topic 으로 메세지 발행
#client.publish('common', json.dumps({"success": "ok"}), 1)
#client.loop_stop()
# 연결 종료
#client.disconnect()
